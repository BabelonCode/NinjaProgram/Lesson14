﻿using System;
using System.Collections.Generic;

namespace _003_HashSet
{
    class Program
    {
        static void Main(string[] args)
        {
            var set = new HashSet<int>();
            set.Add(10);
            set.Add(20);
            set.Add(10);
            set.Add(30);
            set.Add(10);
            set.Add(50);

            set.ExceptWith(new List<int> { 10, 20 });
        }
    }
}
